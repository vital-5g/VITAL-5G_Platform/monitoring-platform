# VITAL-5G Centralised Monitoring Plafrom 

## Describtion 
The role of the VITAL-5G Centralized Monitoring Platform is to collect various types of metrics from the three VITAL-5G testbeds and to make them accessible to other components of the VITAL-5G platform, e.g., the Slice Inventory.

This monitoring platform can monitor metrics that reflect on the (i) network slice performance e.g., latency, peak throughput, average throughput, (ii) service metrics like response time, UE measured throughput, and (iii) infrastructure metrics like CPU load, the full metric list can be found [here](https://docs.google.com/spreadsheets/d/1-67YquHjRGxybjtsgCCyaBOMgEa-YiGHIeac-qmuj0o/edit#gid=1716003726). To retrieve those metrics, the Centralized Monitoring Platform interfaces with the local monitoring systems that are deployed on each of the VITAL-5G testbeds. The local monitoring systems publish collected metrics to a topic specified by the Centralized Monitoring Platform, which then subscribes to that topic and collects these metrics in real-time. The collected metrics are visualized on the Portal Web GUI through dynamically created Grafana dashboards and panels, with each metric creating a new panel on the dashboard, allowing easy monitoring of KPIs and fluctuations. Whenever the `VITAL-5G Service LCM` makes a requeset to collect specific metrics of a specific VITAL-5G testbed the VITAL-5G Centralalised Monitoring Plafrom creates and returns the responsbody in a single JSON body the `topic` name and the `monitoringDashboardUrl` where you can see the Grafana panel collecting the metrics. The main features of the centralized monitor are discussed below:


## How to publish metrics towards the VITAL-5G Centralised Monitoring Platfrom?
You can use the provided [Python](https://gitlab.com/vital-5g/VITAL-5G_Platform/monitoring-platform/-/tree/main/zmq_python_version-master) and [Java](https://gitlab.com/vital-5g/VITAL-5G_Platform/monitoring-platform/-/tree/main/zmq_java_version-master) packages attached to this repo named: [zmq_java_version-master](https://gitlab.com/vital-5g/VITAL-5G_Platform/monitoring-platform/-/tree/main/zmq_java_version-master) and [zmq_python_version-master](https://gitlab.com/vital-5g/VITAL-5G_Platform/monitoring-platform/-/tree/main/zmq_python_version-master). There you will also find for each version a dedicated `README.md`file for it. 
To sommerize the most imporant realted to the ZMQ pub/sub mechanism to interact with the monitoring system: 
- Port `2050`is beining used to push the `service` metric, alongside with its KPI (e.g., averageResponseTime, etc).
- Port `2051` is beining used to push the `platform` metric, alongside with its KPI (e.g., onBoardingDelay, etc).
- Port `2052` is beining used to push the `network` metric, alongside with its KPI (e.g., latency, etc).
- Port `2053` is beining used to push the `infrastructure` metric, alongside with its KPI (e.g., cpuLoad, etc).

The `timestamp` you retrun (with your publisher) needs to be in the `UTC` timezone and thus needs to be collected with the system UTC clock. Thus in this format e.g., `2022-01-21T05:47:26.853Z` (for an example see the provided publisher).

## How to retrieve/subscribe in real-time the metrics that are beining published towards the VITAL-5G Centralised Monitoring Platform?
You need to connect to the IP address of the VITAL-5G Centralised Monitoring Platform so for VITAL-5G - development environment: `172.28.18.102` and for the VITAL-5G production environment: `172.28.18.70` to the port `2054`
You then need to subscribe to the `topic` you want to retrieve the metric from, the `VITAL-5G Service LCM` knows all of these topics.
How the VITAL-5G Centralised Monitoring Platform makes the data available is in the exact same way how the local testbeds publishes its metrics towards the VITAL-5G Centralised Monitoring Platform see this [link](https://docs.google.com/spreadsheets/d/1-67YquHjRGxybjtsgCCyaBOMgEa-YiGHIeac-qmuj0o/edit#gid=1716003726) to have for each metric an example. So again these JSON formats are exactly the same if you want to publish or subscribe data towards the VITAL-5G Centralised Monitoring Plafrom.
### Platform JSON metric example:
```
{
    “timestamp”: "2023-04-07T08:39:33.693677Z",
    “value”: "2.0",
    “vmId”: "db54f2af-f10d-4fdb-b3d6-9480788f5a2a",
    “unit”: “qqq”
}
```
### Network JSON metric example:
```
{
    “timestamp”: "2023-04-07T08:39:33.693677Z",
    “value”: "1.0",
    “unit”: “ms”
    “networkSliceId”: "234542"
}
```
### Service JSON metric example:
```
 {
    “timestamp”: "2023-04-07T08:39:33.693677Z",
    “value”: "4.0",
    “unit”: “%”,
    “vmId”: "db54f2af-f10d-4fdb-b3d6-9480788f5a2a"
}
```
### Infrastructure JSON metric example:
```
 {
    “timestamp”: "2023-04-07T08:39:33.693677Z",
    “value”: "8.0",
    “unit”: “%”,
    “vmId”: "db54f2af-f10d-4fdb-b3d6-9480788f5a2a"
}
```
## How are the topic constructed in the VITAL-5G Centralised Monitoring Platform?
An example of a topic is for examle this, `VSIDc56f37fa-10a6-4f28-af64-9aeca964dd91-antwerp-infrastructure-disk_usage-878371ce-93a3-471f-8496-b5575fb5d5ce`
`verticalServiceId` = `VSIDc56f37fa-10a6-4f28-af64-9aeca964dd91`
`testbed`  = `antwerp`
`metric` = `infrastructure`
`KPI` = `disk_usage`
`slice_id/vm_id` = `878371ce-93a3-471f-8496-b5575fb5d5ce``
it is `sliceId` if it is a `network metric` -> `otherwise` it is the `vmReference`


## Example of the return body of the VITAL-5G Centralised Monitoring Platform when Service LCM made a request:
```
{
    "topic": "VSIDc56f37fa-10a6-4f28-af64-9aeca964dd91-antwerp-infrastructure-disk_usage-878371ce-93a3-471f-8496-b5575fb5d5ce",
    "monitoringDashboardUrl": "http://172.28.18.102:3000/d/dMJqenrVz/c56f37fa-10a6-4f28-af64-9aeca964dd91_infrastructure"
}
```

## How the VITAL-5G Centralalised Monitoring Platform retrieves data from the Result Analytic component?
The Monitoring Platform is over ZMQ subscribed to the port `2055` and the `IP address` of the Result Analytic component. It will also create for that a grafana panel it will subscribe to the topic name provided by the `Service LCM`. The monitoring Platform will also store the received KPIs in the database. 



## Deployment/installation 
Below are the pre requirements prior to run the VITAL-5G Slice Manager and Inventory. 
### Installing PostgreSQL + Monitoring database
1. `sudo apt update` / `sudo apt upgrade`
2. `sudo apt install postgresql postgresql-contrib`
3. `sudo systemctl start postgresql.service`
4. `sudo -i -u postgres`
5. `psql`
6. `CREATE DATABASE monitoringplatform;`
7. `\q`
8. `psql monitoringplatform`
9. `ALTER USER postgres PASSWORD 'postgres';``
Done!

### Installing Java 17 + MAVEN
1. `sudo apt install openjdk-17-jdk-headless`
2. `nano ~/.bashrc`
Provide the following at the end of the file: `# SET JAVA_HOME PATH`
```
JAVA_HOME=/usr/lib/jvm/java-17-openjdk-amd64
export JAVA_HOME
PATH=$JAVA_HOME/bin:$PATH
```
3. ` . ~/.bashrc`
4. `echo $JAVA_HOME`
5. `wget https://repo.maven.apache.org/maven2/org/apache/maven/apache-maven/3.8.6/apache-maven-3.8.6-bin.tar.gz`
6. `tar -xvf apache-maven-3.8.6-bin.tar.gz`
7. `sudo mv apache-maven-3.8.6 /opt/`
8. `sudo chown -R $USER:$USER /opt/apache-maven-3.8.6` 
9. `nano ~/.bashrc` -> Override the `# SET JAVA_HOME PATH` with the following: 
```
JAVA_HOME=/usr/lib/jvm/java-17-openjdk-amd64
MAVEN_HOME=/opt/apache-maven-3.8.6
export JAVA_HOME
export MAVEN_HOME
PATH=$JAVA_HOME/bin:$MAVEN_HOME/bin:$PATH
```
10. `. ~/.bashrc`
11. `echo $MAVEN_HOME`
12 `mvn -v` -> you should see the following: 
```
Java version: 17.0.7, vendor: Private Build, runtime: /usr/lib/jvm/java-17-openjdk-amd64
Default locale: en_GB, platform encoding: UTF-8
OS name: "linux", version: "4.15.0-184-generic", arch: "amd64", family: "unix"
```
Done!

### Installing Grafana
1. `sudo apt-get update` / `sudo apt upgrade`
2. `wget https://dl.grafana.com/enterprise/release/grafana-enterprise_9.0.2_amd64.deb`
3. `sudo apt-get install -y adduser libfontconfig1``
4. `sudo dpkg -i grafana-enterprise_9.0.2_amd64.deb`
5. `sudo nano /etc/apt/sources.list`
6. `sudo service grafana-server start`
Done! You can check with `sudo systemctl status grafana-server`
src/main/resources
Second generate a Grafana token: `https://grafana.com/docs/grafana/latest/developers/http_api/auth/`

### Linking Grafna with the VITAL-5G Centralilsed Monitoring Platform
1. Go to the `src/main/resources` and configure in the `config.properties` file the `grafana.host`, `grafana.port`, `grafana.token`.
Done!

### Running the VITAL-5G Centralised Monitoring Platform:
Just run the JAR file you can find in the directory: JAR 
like this: `java -jar MonitoringPlatfrom-0.0.1-SNAPSHOT.jar`


## Authors and acknowledgment
Vincent Charpentier - imec.

## License
Licensed to imec - closed source-code.

## Q&A
If you have any more questions or doubts please contact:
- `vincent.charpentier@imec.be`
- Or Vincent Charpentier on the VITAL-5G Slack channel
