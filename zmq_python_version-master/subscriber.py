import time

import zmqpubsub


def incoming_data_handler(message_in_json_object, topic_name):
    print(topic_name)
    print(message_in_json_object)
    print("")

    # Here you will come all the data you are subscribed on.


if __name__ == '__main__':
    service_metric_port: str = "2050"
    platform_metric_port = "2051"
    network_metric_port = "2052"
    infrastructure_metric_port = "2053"

    topic: str = "VSIDfc70acf4-3d8c-4a54-9939-ba22a7ea8e38-antwerp-network-latency"
    topic2: str = "VSIDfc70acf4-3d8c-4a54-9939-ba22a7ea8e38-antwerp-network-reliability"

    subscriber = zmqpubsub.Subscriber(ip="0.0.0.0", port=network_metric_port)
    subscriber.subscribe_first_time(topic=topic, callback=incoming_data_handler)  # When it is the first time you do a
    # subscription on your subscriber you always need to use ".subscribe_first_time" since you need to provide the
    # function where you will handle all your incoming data (in this example the "incoming_data_handler" function).
    subscriber.subscribe(topic2)  # When it is your second (or higher) time you want to subscribe to something you
    # always use the function "subscribe"

    for x in range(20):
        time.sleep(1)

    subscriber.unsubscribe(topic)
    print("unsubscribed!")
    time.sleep(1)
    subscriber.stop_subscriber()
    print("subscriber stopped!")
