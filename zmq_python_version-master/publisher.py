import random
import time
import json
from datetime import datetime

import zmqpubsub

if __name__ == '__main__':
    topic: str = "VSIDfc70acf4-3d8c-4a54-9939-ba22a7ea8e38-antwerp-network-latency"
    topic2: str = "VSIDfc70acf4-3d8c-4a54-9939-ba22a7ea8e38-antwerp-network-reliability"

    service_metric_port: str = "2050"
    platform_metric_port = "2051"
    network_metric_port = "2052"
    infrastructure_metric_port = "2053"

    publisher = zmqpubsub.Publisher(ip='0.0.0.0', port=network_metric_port)

    counter = 0

    while counter < 20:
        json_payload = {
                "timestamp": str(datetime.utcnow().isoformat()+'Z'),
                "value": random.randint(10, 15),
                "unit": "ms",
                "networkSliceId": str(3)
            }

        json_object = json.dumps(json_payload)
        publisher.publisher_zmq.publish(topic=topic, payload=json_object)
        publisher.publisher_zmq.publish(topic=topic2, payload=json_object)
        print("sent!")
        time.sleep(1)
        counter = counter+1

    publisher.publisher_zmq.stop()
    
