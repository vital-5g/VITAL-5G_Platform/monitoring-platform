# zmqPubSubJava



## Getting started

In this repo you will find two files namely `MainPublisher.java` and `mainSubscriber.java` and the `zmqpubsub` packge. In order to use the `MainPublisher.java` and to send data back to the centralized monitoring platform. A POST request will be send to the local monitoring system that is running on each testbed. In this POST request (see example below) the topic name will be included along with all other details about the metric, and its metric KPI that needs to be monitored. 
You always want to bind on the wildcard IP `0.0.0.0`
Surrounding the ports:
- `2050`is beining used to push the `service` metric, alongside with its KPI (e.g., averageResponseTime, etc).
- `2051` is beining used to push the `platform` metric, alongside with its KPI (e.g., onBoardingDelay, etc).
- `2052` is beining used to push the `network` metric, alongside with its KPI (e.g., latency, etc).
- `2053` is beining used to push the `infrastructure` metric, alongside with its KPI (e.g., cpuLoad, etc).

The `timestamp` you retrun (with your publisher) needs to be in the `UTC` timezone and thus needs to be collected with the system UTC clock. Thus in this format e.g., `2022-01-21T05:47:26.853Z` (for an example see the provided publisher).

For more details surrounding the JSON data that needs to be pushed towards the centralized monitoring system please visit: `https://docs.google.com/spreadsheets/d/1-67YquHjRGxybjtsgCCyaBOMgEa-YiGHIeac-qmuj0o/edit#gid=1716003726

The `MainPublisher.java` and `mainSubscriber.java` are just examples on how to use the `zmqpubsub` packge. To use the `zmqpubsub` packge you just need to bring that package in your project (wit the pom.xml dependencies). Or you can do it like this: `https://maven.apache.org/guides/mini/guide-3rd-party-jars-local.html`.

## Example of the POST request
This is an example of a POST request that will be send towards your local monitoring system:
`{"verticalServiceInstanceId":"7825aa14-a1bd-4fcb-b38e-9adc418c12c0","unit":"%","useCase":"useCase10","infrastructureMetricType":"CPU_LOAD","metricId":"iotEventAlertManagerCpuLoad","vmReference":"iotEventAlertManager","topic":"VSID7825aa14-a1bd-4fcb-b38e-9adc418c12c0-antwerp-infrastructure-CPU_LOAD","typeOfMetric":"infrastructure","metricKPI":"CPU_LOAD"}`

## Q&A
If you have any more questions or doubts please contact:
- vincent.charpentier@imec.be
- Or Vincent Charpentier on the VITAL-5G Slack channel
