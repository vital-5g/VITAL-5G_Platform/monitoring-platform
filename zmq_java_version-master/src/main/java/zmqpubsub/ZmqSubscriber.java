package zmqpubsub;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;

import java.util.function.BiConsumer;

public class ZmqSubscriber
{
    public ZContext context;
    public ZMQ.Socket socket;
    public String bind;
    public Boolean isRunning;

    public ZmqSubscriber(String ip, String port)
    {
        context = new ZContext();
        socket = context.createSocket(SocketType.SUB);
        bind = "tcp://"+ ip + ":" + port;
    }

    public void subscribeFirstTime(String topic, BiConsumer<JsonObject, String> consumer)
    {
        socket.connect(bind);
        socket.subscribe(topic);
        isRunning = true;
        Thread t = new Thread(() ->
        {
            while (isRunning)
            {
                byte[] recv = socket.recv();
                try
                {
                    String receivedStringMessage = new String(recv);
                    // s1 contains everything after = in the original string (i.e. =....) therefore +1 to remove the =
                    String topicName = receivedStringMessage.substring(0, receivedStringMessage.indexOf(":"));
                    JsonObject jsonObject = (JsonObject) JsonParser.parseString(receivedStringMessage.substring(receivedStringMessage.indexOf(":") + 1));
                    consumer.accept(jsonObject, topicName);
                }
                catch (Exception e)
                {
                    System.out.println("Error: parsing the received data (the data needs to be in JSON format");
                }
            }
        });
        t.setUncaughtExceptionHandler(h);
        t.start();

    }
    Thread.UncaughtExceptionHandler h = new Thread.UncaughtExceptionHandler()
    {
        @Override
        public void uncaughtException(Thread th, Throwable ex)
        {
            System.out.println("subscriber stopped!");

        }
    };

    public void subscribe(String topic)
    {
        socket.subscribe(topic);
    }

    public void unsubscribe(String topic)
    {
        socket.unsubscribe(topic);
    }

    public void stopSubscriber()
    {
        isRunning = false;
        socket.close();
    }
}
