import com.google.gson.JsonObject;
import zmqpubsub.ZmqPublisher;

import java.time.Instant;
import java.util.concurrent.TimeUnit;

public class MainPublisher
{
    public static void main(String[] args) throws Exception
    {
        // jsonObject1 is an example of a payload for the network metric for the others see: https://docs.google.com/spreadsheets/d/1-67YquHjRGxybjtsgCCyaBOMgEa-YiGHIeac-qmuj0o/edit#gid=1716003726
        JsonObject jsonObject1 = new JsonObject();

        String serviceMetricPort = "2050";
        String platformMetricPort = "2051";
        String networkMetricPort = "2052";
        String infrastructureMetricPort = "2053";

        String topic =  "VSIDfc70acf4-3d8c-4a54-9939-ba22a7ea8e38-antwerp-network-latency";
        String topic2 =  "VSIDfc70acf4-3d8c-4a54-9939-ba22a7ea8e38-antwerp-network-reliability";

        Integer max = 15;
        Integer min = 10;

        ZmqPublisher publisher = new ZmqPublisher("0.0.0.0", networkMetricPort);

        for(int i=0; i<10; i++)
        {
            jsonObject1.addProperty("timestamp", Instant.now().toString());
            jsonObject1.addProperty("value", String.valueOf(((Math.random() * (max - min)) + min)));
            jsonObject1.addProperty("unit", "ms");
            jsonObject1.addProperty("networkSliceId", 3);

            publisher.publish(topic, jsonObject1);
            publisher.publish(topic2, jsonObject1);
            System.out.println("Sent!");
            TimeUnit.SECONDS.sleep(1);
        }
    }
}
