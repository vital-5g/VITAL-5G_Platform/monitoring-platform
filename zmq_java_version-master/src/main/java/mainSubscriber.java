import com.google.gson.JsonObject;
import zmqpubsub.ZmqSubscriber;

import java.util.concurrent.TimeUnit;

public class mainSubscriber
{
    protected static void incomingDataHandler(JsonObject jsonObject, String topicName)
    {
        System.out.println(topicName);
        System.out.println(jsonObject);

        // Here you will come all the data you are subscribed on.
    }

    public static void main(String[] args) throws InterruptedException
    {
        String serviceMetricPort = "2050";
        String platformMetricPort = "2051";
        String networkMetricPort = "2052";
        String infrastructureMetricPort = "2053";

        String topic = "VSIDfc70acf4-3d8c-4a54-9939-ba22a7ea8e38-antwerp-network-latency";
        String topic2 = "VSIDfc70acf4-3d8c-4a54-9939-ba22a7ea8e38-antwerp-network-reliability";

        ZmqSubscriber subscriber = new ZmqSubscriber("0.0.0.0", networkMetricPort);
        subscriber.subscribeFirstTime(topic, mainSubscriber::incomingDataHandler); // When it is the first time you do a
        // subscription on your subscriber you always need to use ".subscribeFirstTime" since you need to provide the
        // method where you will handle all your incoming data (in this example the "incomingDataHandler" method).

        subscriber.subscribe(topic2); // When it is your second (or higher) time you want to subscribe to something you
        // always use the function "subscribe"


        for(int i=0; i < 5; i++)
        {
            TimeUnit.SECONDS.sleep(1);
        }

        subscriber.unsubscribe(topic);
        System.out.println("unsubscribed");
        subscriber.stopSubscriber();
    }
}
